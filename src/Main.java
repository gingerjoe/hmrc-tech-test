import java.util.*;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class Main {

    private static final int MAX_NUM_ITEMS_IN_LIST = 40;

    public static void main(String[] args) {
        //apples = 60p, oranges = 25p
        Map<String, BOM> bomHashmap = new HashMap<>();
        bomHashmap.put("Apple", new BOM("Apple", 60, BOM.SpecialOffers.BOGOF));
        bomHashmap.put("Orange", new BOM("Orange", 25, BOM.SpecialOffers.THREEFORTWO));

        //generate offer function map
        Map<BOM.SpecialOffers, BasicInterface> offerMap = new HashMap<>();
        offerMap.put(BOM.SpecialOffers.BOGOF, Main::Bogof);
        offerMap.put(BOM.SpecialOffers.THREEFORTWO, Main::ThreeForTwo);
        
        //generate random list of items
        List<String> BomItems = new ArrayList<>();
        Random rng = new Random();
        for (int i = 0; i < rng.nextInt(MAX_NUM_ITEMS_IN_LIST); i++) {
            int randomIndex = rng.nextInt(bomHashmap.size());
            String bomItem = (String)bomHashmap.keySet().toArray()[randomIndex];
            BomItems.add(bomItem);
        }

        //scan and quantise items
        Map<String, Integer> bomItemQuantities = BomItems.stream().collect(groupingBy(Function.identity(), summingInt(x -> 1)));

        //output total cost
        long totalCost = GetTotalCost(bomHashmap, bomItemQuantities, offerMap);

        System.out.printf("Total price = £%3d.%2d%n", totalCost/100, totalCost%100);
    }

    private static long GetTotalCost(Map<String, BOM> bomHashmap, Map<String, Integer> bomItemQuantities, Map<BOM.SpecialOffers, BasicInterface> offerMap) {
        long totalCost = 0;
        for (String itemID: bomItemQuantities.keySet()) {

            int itemQuantity = bomItemQuantities.get(itemID);
            long itemPrice = bomHashmap.get(itemID).GetPrice();
            long itemTotalCost = itemQuantity * itemPrice;
            long itemTotalCostAfterDiscounts = itemTotalCost;

            System.out.printf("%10s @ £%3d.%2d * %2d = £%3d.%02d%n", itemID, itemPrice/100, itemPrice%100, itemQuantity, itemTotalCost/100, itemTotalCost%100);

            if (bomHashmap.get(itemID).GetSpecialOffer() != BOM.SpecialOffers.NONE){
                itemTotalCostAfterDiscounts = offerMap.get(bomHashmap.get(itemID).GetSpecialOffer()).ApplyOffer(itemQuantity, itemPrice);
                long totalDeduction = itemTotalCost - itemTotalCostAfterDiscounts;
                System.out.printf("%-25s = £%3d.%02d%n", "Discount Applied", totalDeduction/100, totalDeduction%100);
            }

            totalCost += itemTotalCostAfterDiscounts;
        }
        return totalCost;
    }

    @FunctionalInterface
    public static interface BasicInterface {
        long ApplyOffer(int q, long p);
    }

    public static long ApplyOffer(int q, long p){
        return 0;
    }

    public static interface BogofInterface extends BasicInterface {
        long Bogof(int q, long p);
    }

    public static long Bogof(int quantity, long price){
        int div = quantity / 2;
        int mod = quantity % 2;
        return (div + mod) * price;
    }

    public static interface ThreeForTwoInterface extends BasicInterface {
        long ThreeForTwo(int q, long p);
    }

    public static long ThreeForTwo(int quantity, long price){
        int div = quantity / 3;
        int mod = quantity % 3;
        return ((div * 2) + mod) * price;
    }
}