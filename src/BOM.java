import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ellipsis on 22/11/2017.
 */
public class BOM {
    //Bill of Materials
    private long Price;
    private String Name;
    private SpecialOffers SpecialOffer;

    public SpecialOffers GetSpecialOffer() {
        return SpecialOffer;
    }

    public void SetSpecialOffer(SpecialOffers specialOffer) {
        SpecialOffer = specialOffer;
    }

    public enum SpecialOffers {
        NONE,
        BOGOF,
        THREEFORTWO
    }

    public BOM(String id, long price, SpecialOffers specialOffer){
        Name = id;
        Price = price;
        SpecialOffer = specialOffer;
    }

    public long GetPrice() {
        return Price;
    }

    public void SetPrice(long price) {
        Price = price;
    }

    public String GetName() {
        return Name;
    }

    public void SetName(String name) {
        Name = name;
    }

}
